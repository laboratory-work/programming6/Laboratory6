package ru.lanolin;

import ru.lanolin.lib.fete.Fete;
import ru.lanolin.lib.human.Human;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Client {

    private Socket socket;
    private final String hostname;
    private final int port;
    private final long heartbeatDelayMillis = 2000;
    private final long deltaSleep = 50;
    private final int maxTryingToReconnect = 5;

    {
        //TODO: read in the file
        hostname = "localhost";
        port = 2504;
    }

    public Client() {  }

    public void connect() throws IOException{
        try {
            socket = new Socket(hostname, port);
            System.out.println("Успешно подключено");
        } catch (IOException e) {
            System.err.println("Не удалось подключиться. " + e.getLocalizedMessage());
            throw e;
        }
    }

    public boolean reconnect(){
        for (int attempt = 1; attempt <= maxTryingToReconnect; attempt++) {
            try {
                socket = new Socket(hostname, port);
                System.out.println("Соединение восстановлено");
                return true;
            } catch (IOException e) {
                if(attempt == 1) System.out.println("\n");
                System.err.println((char) 27 + "[1A"
                        + "Попытка " + attempt + ": " + e.getLocalizedMessage());
                long currentTime = 0L;
                while(currentTime < heartbeatDelayMillis){
                    currentTime += deltaSleep;
                    try {
                        Thread.sleep(deltaSleep);
                    } catch (InterruptedException e1) {
                        //e1.printStackTrace();
                    }
                }
            }
        }
        System.err.println("В соеденении отказано. Попытки кончились");
        shutdown();
        return false;
    }

    public void reciveVectorHuman(Fete fete){
        try {
            InputStream is = this.socket.getInputStream();
            for (int loop = 0; loop < 5; loop++) {
                int ava = is.available();
                if(ava > 0) {
                    byte[] inputData = new byte[ava];
                    is.read(inputData);
                    ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(inputData);
                    ObjectInputStream objectInputStream = new ObjectInputStream(arrayInputStream);
                    Object msg = objectInputStream.readUnshared();
                    if(Objects.nonNull(msg)) {
                        ArrayList<Human> guest = (ArrayList<Human>) msg;
                        fete.addParticipants(guest);
                        break;
                    }
                }else{
                    Thread.sleep(500);
                }
            }
        }catch (EOFException eoef) {
            System.err.println("Отвал сервера");
        } catch (IOException | ClassNotFoundException sexep){
            System.err.println("Ошибка при чтении. (" + sexep.getLocalizedMessage() + ")");
            sexep.printStackTrace();
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    public void shutdown() {
        System.out.println("Прекращается работа");
        try {
            if(socket != null) socket.close();
        } catch (IOException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

}

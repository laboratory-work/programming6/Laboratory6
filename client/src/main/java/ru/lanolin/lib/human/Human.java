package ru.lanolin.lib.human;

import ru.lanolin.lib.enums.Feel;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;

/**
 * Описнаие участника мероприятия
 */
public abstract class Human implements DoHuman, Comparable<Human>, Serializable, Comparator<Human> {

    private static final long serialVersionUID = -2186644049879053315L;
    /**
     * Имя человека
     */
    private String name;

    /**
     * Настроение человека
     */
    private Feel feel;

    /**
     * Качество наряда на человеке
     */
    private int quality;

    /**
     * Дата инициализации коллекции
     */
    private LocalDate birthday;

    public Human(String name, Feel feel, int quality, LocalDate date) {
        this(name, feel);
        this.quality = quality;
        this.birthday = date;
    }

    public Human(String name, Feel feel) {
        this();
        this.feel = feel;
        this.name = name;
    }

    protected Human() {
        birthday = LocalDate.now();
        this.quality = 1;
    }

    @Override
    public String toString() {
        return String.format("<human><name>%s</name><fate>%s</fate><quality>%d</quality><birthday>%s</birthday></human>",
                name, feel.toString(), quality, birthday.toString());
    }

    /**
     * Выводит информацию о {@link Human} в приятной виде
     *
     * @return {@link String}
     */
    public String toPrettyString() {
        return String.format("Name: %s, Fate: %s, Quality: %d, Birthday: %s",
                name, feel.toString(), quality, birthday.toString());
    }

    @Override
    public int hashCode() {
        int sum = toString().hashCode();
        if (feel == Feel.CONSTRAINT) sum += 5 * 101;
        else if (feel == Feel.NONE) sum += 2 * 101;
        else if (feel == Feel.OK) sum += 3 * 101;

        return sum;
    }

    /**
     * Сравнивает двух {@link Human}. Сначало именя, потом натсроение, потом качество и последнее - оценка
     *
     * @param another {@link Human}
     * @return -1 if {@code this} less {@code another} <br>
     * 0 if {@code this} equals {@code another} <br>
     * 1 if {@code this} grate {@code another}
     */
    @Override
    public int compare(Human o1, Human another) {
        int compare = o1.name.compareTo(another.getName());
        if (compare != 0) return compare;

        compare = o1.getBirthday().compareTo(another.getBirthday());
        if (compare != 0) return compare;

        compare = o1.feel.compareTo(another.getFeel());
        if (compare != 0) return compare;

        return Integer.compare(o1.quality, another.getQuality());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;
        Human h = (Human) obj;
        return this.feel == h.getFeel() &&
                this.name.equals(h.getName()) &&
                this.quality == h.getQuality() &&
                this.birthday == h.getBirthday();
    }

    /**
     * Сравнивает двух {@link Human} по оценке за наряд
     *
     * @param another {@link Human}
     * @return -1 if {@code this} less {@code another} <br>
     * 0 if {@code this} equals {@code another} <br>
     * 1 if {@code this} grate {@code another}
     */
    @Override
    public int compareTo(Human another) {
        return Integer.compare(this.getQuality(), another.getQuality());
    }

    /**
     * Оценит наряд другого {@link Human}.
     *
     * @param other {@link Human}
     */
    public abstract void rateOutfit(Human other);

    public Feel getFeel() {
        return feel;
    }

    public void setFeel(Feel feel) {
        this.feel = feel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = LocalDate.parse(birthday);
    }

    public LocalDate getBirthday() {
        return birthday;
    }
}
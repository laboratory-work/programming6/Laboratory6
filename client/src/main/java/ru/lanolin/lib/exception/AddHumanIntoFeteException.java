package ru.lanolin.lib.exception;

public class AddHumanIntoFeteException extends RuntimeException {

    private String message;

    public AddHumanIntoFeteException() {
        message = "Добавили участника, который уже есть на мероприятии. Клонирование не позволительно и запрещено.";
    }

    public AddHumanIntoFeteException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

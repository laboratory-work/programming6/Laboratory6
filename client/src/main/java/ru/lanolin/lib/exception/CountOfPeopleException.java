package ru.lanolin.lib.exception;

public class CountOfPeopleException extends Exception{

    private String message;

    public CountOfPeopleException() {
        message = "Игра в жизнь провалена. Он умер от одиночества.";
    }

    public CountOfPeopleException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package ru.lanolin.lib.enums;

/**
 * Чувство {@link ru.lanolin.lib.human.Human} на мероприятии
 */
public enum Feel {
    CONSTRAINT, //Чувствовать себя Стесненно
    OK, //Чувстовать себя Хорошо
    NONE //Чувствовать себя Никак
}
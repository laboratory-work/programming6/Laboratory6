package ru.lanolin.lib.fete;

import ru.lanolin.lib.exception.CountOfPeopleException;
import ru.lanolin.lib.human.Human;

import java.util.List;
import java.util.Vector;

public interface DoFete {

    /**
     * Добавить участника на мероприятие
     *
     * @param h {@link Human}, которого хотят добавить на мероприятие
     */
    void addParticipant(Human h);

    /**
     * Уйти с мероприятия
     *
     * @param h {@link Human}, которого хотят уйти с мероприятия
     */
    void leaveParticipant(Human h);

    /**
     * Добавить на мероприятие множество {@link Human}
     *
     * @param hs {@link Vector} содержащий объекты типа {@link Human}
     */
    default void addParticipants(List<Human> hs) {
        hs.forEach(this::addParticipant);
    }

    /**
     * Убрать множество гостей с мероприятие
     *
     * @param guest {@link Vector} содержащий объекты типа {@link Human}.
     */
    default void leaveParticipants(List<Human> guest) {
        for (int i = guest.size() - 1; i > -1; i--) {
            this.leaveParticipant(guest.get(i));

        }
    }

    /**
     * Начать мероприятие
     */
    void startFete() throws CountOfPeopleException;

    /**
     * Закончить мероприятие
     */
    void stopFate();
}

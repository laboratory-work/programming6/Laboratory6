package ru.lanolin.lib.fete;

import ru.lanolin.lib.enums.Phrases;
import ru.lanolin.lib.enums.Situation;
import ru.lanolin.lib.exception.CountOfPeopleException;
import ru.lanolin.lib.human.Human;
import ru.lanolin.util.Utils;

import java.util.*;

/**
 * Класс, описывающий Мероприятие
 */
public abstract class Fete implements DoFete {

    /**
     * Коллекция, в которой храянтся все {@link Human}
     */
    protected final List<Human> guests;
    private ArrayList<DoLater> doLaters = new ArrayList<>();

    private Situation situation;
    private boolean startFete = false;

    private final Controller controller;

    public Fete(Situation situation){
        this.guests = Collections.synchronizedList(new ArrayList<Human>(Utils.Const.SIZE));
        this.situation = situation;
        controller = new Controller();
    }

    /**
     * Метод, возвращающий {@link Vector} гостей, присутствующих на мероприятии.
     *
     * @return {@link Vector}
     */
    public List<Human> getGuests() {
        return guests;
    }

    /**
     * Получить окружающую обстановку
     */
    public Situation getSituation() {
        return situation;
    }

    public void addDoLater(DoLater dolate) {
        if (!doLaters.contains(dolate) && dolate != null)
            doLaters.add(dolate);
    }

    public void setSituation(Situation s) {
        situation = s;
        System.out.println(">>Окружающая обстановка изменилась на " + s);
        if (startFete && s != Situation.RAIN) {
            List<Human> getRandomPeople = Utils.pickNRandomElements(guests);
            Human h1 = getRandomPeople.get(0);
            Human h2 = getRandomPeople.get(1);
            h1.repeat(h2, Phrases.FEELGOOD);
        }
    }

    private void tick(long time) {

        if ((time / Utils.Const.SECOND) > Utils.Const.WorkTimeS) {
            this.stopFate();
            return;
        }

        for (DoLater dl : doLaters)
            dl.complete(time / 1000000);

        List<Human> getRandomPeople = Utils.pickNRandomElements(guests);
        Human h1 = getRandomPeople.get(0);
        Human h2 = getRandomPeople.get(1);

        int randDO = new Random().nextInt(4);

        if (randDO == 0) {
            h1.great(h2);
        } else if (randDO == 1) {
            h1.bowOut(h2);
        } else if (randDO == 2) {
            h1.rateOutfit(h2);
        } else if (randDO == 3) {
            Phrases phr = Utils.getRandomElementEnum(Phrases.class);
            do {
                if (getSituation() == Situation.RAIN && phr == Phrases.FEELGOOD)
                    phr = Utils.getRandomElementEnum(Phrases.class);
                else break;
            } while (true);
            h1.repeat(h2, phr);
        }
    }

    /**
     * Началось ли мероприятие
     */
    public boolean isStartFete() {
        return startFete;
    }

    /**
     * Установить началось ли мероприятие
     */
    public void setStartFete(boolean startFete) {
        this.startFete = startFete;
    }

    public int sizeGuest() {
        return guests.size();
    }

    public Controller getController(){
        return controller;
    }

    public class Controller {

        private boolean isRunning;

        Controller(){
            isRunning = false;
        }

        public void start() {
            if(isRunning)
                return;
            try {
                startFete();
                run();
            }catch (CountOfPeopleException c){ }
        }

        void stop(){
            if(!isRunning)
                return;
            isRunning = false;
        }

        private void run(){
            isRunning = true;

            final double frameTime = 1.0 / Utils.Const.FRAMECAP;

            long lastTime = Utils.Time.getTime();
            final long startGame = lastTime;
            double unprocessedTime = 0;

            while(isRunning) {

                boolean render = false;

                long startTime = Utils.Time.getTime();
                long passedTime = startTime - lastTime;
                lastTime = startTime;

                unprocessedTime += passedTime / (double) Utils.Const.SECOND;

                while((unprocessedTime - frameTime) > 1e-8) {
                    render = true;
                    unprocessedTime -= frameTime;
                    if(!isStartFete()) { stop(); return; }
                }

                if(render) {
                    tick(lastTime - startGame);
                }else {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}

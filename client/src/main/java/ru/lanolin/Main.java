package ru.lanolin;

import ru.lanolin.additions.Celebrate;
import ru.lanolin.lib.enums.Situation;
import ru.lanolin.util.Utils;

import java.io.IOException;

public class Main {

    public static Celebrate celebrate;
    public static Client client;

    public static void main(String[] args){
        generateStory();

        client = new Client();
        System.out.println("Попытка подключиться к серверу на " + client.getPort() + " порту");

        try {
            client.connect();
        }catch (IOException e){
            if(!client.reconnect()) return;
        }
//        Thread.sleep(500);
        client.reciveVectorHuman(celebrate);
//        System.out.println("celebrate = " + celebrate.getGuests());
        celebrate.getController().start();
    }

    //{"name":"Haiden Saunders","fate":"CONSTRAINT", "quality": 2, "evaluation":55}

    private static void generateStory() {
        celebrate = new Celebrate(Situation.SUN);

        celebrate.addDoLater(e -> {
            if(Utils.Time.isWork(e - 3000)){
                celebrate.setSituation(Situation.SUN);
            }
        });
    }
}

package ru.lanolin.additions;

import ru.lanolin.lib.exception.AddHumanIntoFeteException;
import ru.lanolin.lib.exception.LeaveHumanIntoFeteException;
import ru.lanolin.lib.fete.Fete;
import ru.lanolin.lib.human.Human;

import java.io.File;

public class Celebrate extends Fete {

    public Celebrate(File envFile) {
        super(envFile);
    }

    @Override
    public void addParticipant(Human h) {
        if (this.guests.contains(h))
            throw new AddHumanIntoFeteException();
        this.guests.add(h);
    }

    @Override
    public void leaveParticipant(Human h) {
        if (!this.guests.contains(h))
            throw new LeaveHumanIntoFeteException();
        this.guests.remove(h);
    }
}

package ru.lanolin;

import ru.lanolin.thread.OneClientAnswThread;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class Server extends Thread{

    private static final InetSocketAddress socketAddress = new InetSocketAddress(2504);
    private Selector selector;
    private ServerSocketChannel socketChannel;
    private ArrayList<SocketChannel> avaibale = new ArrayList<>();

    public Server() {
        super("[Server]");
        try {
            selector = Selector.open();

            socketChannel = ServerSocketChannel.open();
            socketChannel.bind(socketAddress);
            socketChannel.configureBlocking(false);
            socketChannel.register(selector, socketChannel.validOps(), null);

            System.out.println("Сервер запущен");
        } catch(BindException be){
            System.err.println("Внимание! " + be.getLocalizedMessage());
            try {
                selector.close();
                socketChannel.close();
            } catch (IOException e) {  }
        } catch (IOException e) {
            e.printStackTrace();//TODO
        }
    }

    public boolean isConnect(){
        return selector.isOpen() && socketChannel.isOpen();
    }

    @Override
    public void run() {
        super.run();
        try{
            while (selector.isOpen() && socketChannel.isOpen() && !isInterrupted()) {
                selector.select();
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> selectionKeyIterator = selectionKeys.iterator();

                while (selectionKeyIterator.hasNext() && selector.isOpen() && socketChannel.isOpen()) {
                    SelectionKey myKey = selectionKeyIterator.next();

                    if (myKey.isAcceptable()) {
                        SocketChannel socketClient = socketChannel.accept();
                        socketClient.configureBlocking(false);
                        socketClient.register(selector, SelectionKey.OP_WRITE);
                        System.out.println("Connection Accepted: " + socketClient.getRemoteAddress());
                    }else if(myKey.isWritable()){
                        SocketChannel sc = (SocketChannel) myKey.channel();
                        if(!avaibale.contains(sc)) {
                            new OneClientAnswThread(selector, sc).start();
                            avaibale.add(sc);
                        }
                    }
                    selectionKeyIterator.remove();
                }
            }
        }catch (IOException e){
            e.printStackTrace();//TODO
        }
    }

    @Override
    public void interrupt() {
        super.interrupt();
        try {
            if(!socketChannel.isOpen()) socketChannel.close();
            if(!selector.isOpen()) selector.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

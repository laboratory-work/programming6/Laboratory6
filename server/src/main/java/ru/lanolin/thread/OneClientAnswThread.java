package ru.lanolin.thread;

import ru.lanolin.Main;
import ru.lanolin.lib.human.Human;
import ru.lanolin.util.Utils;

import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

public class OneClientAnswThread extends Thread {

    private SocketChannel client;
    private Selector selector;
    private ArrayList<Human> vectorHuman;

    public OneClientAnswThread(Selector selector, SocketChannel client) {
        super("[ServerWriter]");
        this.client = client;
        this.selector = selector;
        vectorHuman = new ArrayList<>(Main.celebrate.getGuests());
        vectorHuman.sort(Human::compareTo);
    }

    @Override
    public void run() {
        super.run();
        try{
            client.write(Utils.convertObject2Buffer(vectorHuman));
            client.register(selector, SelectionKey.OP_CONNECT);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}

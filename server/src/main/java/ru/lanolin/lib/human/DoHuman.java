package ru.lanolin.lib.human;

import ru.lanolin.lib.enums.Phrases;

public interface DoHuman {

    /**
     * Поприветсвовать другого участника
     *
     * @param other {@link Human} другой человек
     */
    void great(Human other);

    /**
     * Раскланиваться с другим участником
     *
     * @param other {@link Human} другой человек
     */
    void bowOut(Human other);


    /**
     * Повторить особенную фразу другому участнику
     * @param other {@link Human} другой человек, которому говорят фразу
     * @param phrase {@link Phrases} фраза, которую говорят
     */
    default void repeat(Human other, Phrases phrase) {
        System.out.println(">" + this + " обращается к " + other.toString() + " и говорить: " + phrase.getPhrases());
    }

}

package ru.lanolin.lib.fete;

import ru.lanolin.additions.Guest;
import ru.lanolin.lib.enums.Feel;
import ru.lanolin.lib.exception.AddHumanIntoFeteException;
import ru.lanolin.lib.exception.LeaveHumanIntoFeteException;
import ru.lanolin.lib.human.Human;
import ru.lanolin.util.Utils;
import ru.lanolin.util.XML;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Класс, описывающий Мероприятие
 */
public abstract class Fete implements DoFete {

    /**
     * Коллекция, в которой храянтся все {@link Human}
     */
    protected final List<Human> guests;
    private boolean invertSort;

    public Fete(File envFile) {
        this.guests = Collections.synchronizedList(new ArrayList<Human>(Utils.MAXSIZE));
        load(envFile);
        invertSort = false;
    }

    /**
     * Метод, который парсит с помощью {@link XML} файл с описанием каждого {@link Human}.
     * Всех сущностей загружает в {@link Vector}.
     * @param envFile {@link File} файл, в котором хранятся xml данные
     */
    private void load(File envFile) {
        XML xmlParser = new XML(envFile);

        List<Human> guests = null;
        try {
            guests = xmlParser.readXMLFile();
        } catch (IOException | XMLStreamException e) {
            System.err.println("Произошла ошибка при чтении файла.\nОшибка " + e.getClass().getCanonicalName()
                    + " с сообщением: " + e.getLocalizedMessage());
            System.exit(-1);
        }

        addParticipants(guests);
    }

    /**
     * Метод, возвращающий {@link List} гостей, присутствующих на мероприятии.
     *
     * @return {@link List}
     */
    public List<Human> getGuests() {
        return guests;
    }

    /**
     * Специальный мемтод, который создает объект типа {@link Human} по {@link Map}, в котором хранятся
     * поля со значениями.
     *
     * @param element {@link Map} с возможными полями:
     *                <ul>
     *                <li>{@code name},{@code fate} - обязательные поля</li>
     *                <li>{@code quality},{@code birthday} - опциональные поля</li>
     *                </ul>
     * @return соданный объект типа {@link Human} из переданных данных в параметр.
     */
    private Human construct(Map<String, Object> element) {
        Human h = null;

        if (Objects.nonNull(element))
            if (element.size() == 4) {
                try {
                    h = new Guest(
                            element.get("name").toString(),
                            Feel.valueOf(element.get("fate").toString()),
                            Integer.parseInt(element.get("quality").toString()),
                            element.get("birthday").toString()
                    );
                } catch (NullPointerException e) {
                    System.err.println("Не хватает одного из полей или непральное имя поля в ключе");
                } catch (NumberFormatException e1) {
                    System.err.println("Внимание, передано значение в поле, требующие числовое значение, " +
                            "которое не переводится в числовое");
                }
            } else if (element.size() == 2 &&
                    element.containsKey("name") && element.containsKey("fate")) {
                try {
                    h = new Guest(element.get("name").toString(), Feel.valueOf(element.get("fate").toString()));
                } catch (NullPointerException e) {
                    System.err.println("Не хватает одного из полей или непральное имя поля в ключе");
                } catch (IllegalArgumentException e1) {
                    System.err.println("В качестве значения Fate было передано такое, которое не существует");
                }
            }

        return h;
    }

    /**
     * Команда в консоль: {@code info} <br>
     * Выводит в консоль информацию о коллекции, в которой хранятся гости на мероприятии.<br>
     * Информация:
     * <ul>
     * <li>тип коллекции;</li>
     * <li>дата инициализации;</li>
     * <li>количесво элементом;</li>
     * <li>максимальное количество элементов;</li>
     * <li>заполенность коллекции.</li>
     * </ul>
     */
    public void info() {
        String builder = String.join("", Collections.nCopies(10, "-")) + System.lineSeparator() +
                "Информация о коллекции: " + System.lineSeparator() +
                String.format("Тип: %s", guests.getClass().getCanonicalName()) + System.lineSeparator() +
//                String.format("Время инициализации: %s", initialize) + System.lineSeparator() +
                String.format("Максимальное количество элементов: %d", Utils.MAXSIZE) + System.lineSeparator() +
                String.format("Количество элементов в коллекции: %d", guests.size()) + System.lineSeparator() +
                String.format("Заполнение %3.2f%%", (float) guests.size() / Utils.MAXSIZE * 100) + System.lineSeparator() +
                String.join("", Collections.nCopies(10, "-")) + System.lineSeparator();
        System.out.println(builder);
    }

    /**
     * Команда в консоль: {@code reorder} <br>
     * Действие: производит сортировку коллекции в обратном порядке.
     */

    public void reorder() {
        guests.sort((h1, h2) -> invertSort ? -h1.compareTo(h2) : h1.compareTo(h2) );
        System.out.println("Сортировка выполнена. Колекция отсортирована в порядке " +
                        (invertSort ? "убывания" : "возрастания"));
    }
    
    /**
     * Команда в консоль: {@code help} <br>
     *
     */
    public void help(){
        String sb = "Справка по командам:" + System.lineSeparator() +
                "{JSON element} - обязательное содержание ключей 'name' и 'fate', " +
                "а также необязательные элементы тип int: 'quality' и 'evaluation'" + System.lineSeparator() +
                "Доступные варианты для ввода в поле 'fate': " + Arrays.asList(Feel.values()).toString() + System.lineSeparator() +
                ">reorder - команда сортирует коллекцию в обратном порядке" + System.lineSeparator() +
                ">info - команда выводит информацию об коллекции" + System.lineSeparator() +
                ">show - команда показывает все элементы коллекции" + System.lineSeparator() +
                ">start - команда запускает генерацию сценария" + System.lineSeparator() +
                ">remove_greater {JSON element} - команда принимает на вход JSON строку, в которой описан " +
                "элемент с которого будет удалены элемента большие этого" + System.lineSeparator() +
                ">add {JSON element} - команда добавляет в коллекцию объект, составленный из JSON строки." + System.lineSeparator() +
                ">remove {JSON element} - удаляет элемент из коллекции." + System.lineSeparator() +
                ">insert {int index} {JSON element} - Добавляет элемент в index позицию." + System.lineSeparator() +
                ">exit/close - Завершение работы приложения." + System.lineSeparator();
        System.out.println(sb);
    }
    
    /**
     * Команда в консоль: {@code remove_greater} {JSON element} <br>
     * Удаляте из коллекции элементы, большие данного
     *
     * @param element {@link String} JSON строка.
     */
    public void remove_greater(String element) {
        final Human h = construct(Utils.parseJSON(element));

        if (Objects.isNull(h)) {
            String m = "Внимание, невалидный объект не может быть найден в коллекции";
            System.err.println( m);
            return;
        }

        if (guests.indexOf(h) != -1) {
            guests.removeIf(human -> human.compareTo(h) < 0);
            System.out.println("Удаление выполнено");
        } else {
            System.err.println("Переданный элемент не может быть найден в коллекции. " +
                    "Либо его нет в коллекции, либо уточните запрос");
        }
    }

    /**
     * Команда в консоль: {@code add} {JSON element} <br>
     * Добавляет элемент в коллецию.
     * При ошибке в JSON строке - данный объект не будет добвален в коллекцию
     *
     * @param element JSON строка, которая должна содержать: <ul>
     *                <li>{@code name},{@code fate} - обязательные поля</li>
     *                <li>{@code quality},{@code evaluation} - опциональные поля</li>
     *                </ul>
     */
    public void add(String element) {
        final Human h = construct(Utils.parseJSON(element));
        
        if (Objects.isNull(h)) {
            System.err.println("Внимание, невалидный объект не может быть добавлен в коллекцию");
            return;
        }
        try {
            addParticipant(h);
        }catch (AddHumanIntoFeteException add){
            System.err.println( add.getMessage());
            return;
        }
        String m = "Успешно добавлен";
        System.out.println(m);
    }

    /**
     * Команда в консоль: {@code remove} {JSON element} <br>
     * По переданным параметрам в JSON виде, удаляется элеммент из коллекции, если это возможно. <br>
     * Чтобы точно удалить элемент из коллекции, нужно передать точные значения полей.
     *
     * @param element {@link String} JSON строка, которая должна содержать:
     *                <ul>
     *                <li>{@code name},{@code fate} - обязательные поля</li>
     *                <li>{@code quality},{@code evaluation} - опциональные поля</li>
     *                </ul>
     */
    public void remove(String element) {
        final Human h = construct(Utils.parseJSON(element));

        if (Objects.isNull(h)) {
            System.err.println("Внимание, невалидный объект не может быть удален из коллекцию");
            return;
        }

        try {
            leaveParticipant(h);
        }catch (LeaveHumanIntoFeteException lhe){
            System.err.println(lhe.getMessage());
            return;
        }
        System.out.println("Успешно удален");
    }

    /**
     * Команда в консоль: {@code show}<br>
     * Такая команда выводит в консоль все элементы в "красивом виде".
     */
    public void show() {
        StringBuilder builder = new StringBuilder("Вывод всех элементов коллекции: \n");
        guests.forEach(h -> builder.append(h.toPrettyString()).append(System.lineSeparator()));
        System.out.println(builder.toString());
    }

    /**
     * Команда в консоль: {@code insert} {{@code int} index} {JSON element} <br>
     * По JSON создается элемент типа {@link Human}, и добавляется в позицияю, переданную в {@code index}.
     *
     * @param text {@link String} состоящий из двух частей, разделенные пробелом: {{@code int} index} {{@code JSON} element}
     */
    public void insert(String text) {
        String[] splitText = text.split("\\s+", 2);
        int position;
        try {
            position = Integer.parseInt(splitText[0]);
        } catch (NumberFormatException e) {
            System.err.println("В качестве индекса для вставки элемента было передано невалидное значение. " +
                    "Вставить элемент в коллекцию неудалось");
            return;
        }
    
        final Human h = construct(Utils.parseJSON(splitText[1]));

        if (Objects.isNull(h)) {
            System.err.println("Внимание, невалидный объект не может быть вставлен в коллекцию");
            return;
        }

        if(guests.contains(h)){
            System.err.println("Внимание, " + new AddHumanIntoFeteException().getMessage());
            return;
        }

        try {
            guests.add(position, h);
        } catch (IndexOutOfBoundsException e) {
            System.err.println("В качествее индекса было введено значения, которое находится вне дозволенных границ." +
                    "Дозволенные границы: [0;" + guests.size() + ")");
        }
    }
}
